const { Resolver } = require('dns');
const {SMTPClient} = require('smtp-client');

async function smtp(){
    const host_name = 'cybernetyx.com'
    let s = new SMTPClient({
        host: 'smtp.gmail.com',
        port: 25,
        secure: true
    });
    console.log(await s.connect())
    console.log(await s.greet({host_name}))
    console.log(await s.secure())
}

smtp()
.then(r=>{
    console.log(r);
})
.catch(e=>{
    console.log(e);
})
